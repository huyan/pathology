/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   kfb_reader.h
 * Author: huyan <huyan@rimag.com.cn>
 *
 * Created on 2017年8月8日, 上午11:32
 */

#ifndef KFB_READER_H
#define KFB_READER_H

#ifdef __cplusplus
extern "C" {
#endif

typedef struct IMAGE_INFO_STRUCT {
    int DataFilePTR;
} ImageInfoStruct; //Create structure

typedef signed int KF_INT32;
typedef void *LPVOID;

//初始化图像
typedef int (*DLLInitImageFileFunc)(ImageInfoStruct*, const char*);
//获取头信息Define pointer function type
typedef int (*DLLGetHeaderInfoFunc)(ImageInfoStruct, KF_INT32*, KF_INT32*, KF_INT32*, float*, double*, float*, KF_INT32*);
//获取图像流
typedef unsigned char* (*DLLGetImageStreamFunc)(ImageInfoStruct*, float, KF_INT32, KF_INT32, KF_INT32*, unsigned char**);
//获取RGB数据流
typedef unsigned char* (*DLLGetRGBDataImageStreamFunc)(ImageInfoStruct*, float, KF_INT32, KF_INT32, KF_INT32*, KF_INT32*, KF_INT32*, unsigned char**); //Define pointer function type
//获取指定区域图像数据流
typedef int (*DLLGetImageDataRoiFunc)(ImageInfoStruct, float, KF_INT32, KF_INT32, KF_INT32, KF_INT32, unsigned char**, KF_INT32*, KF_INT32); //Define pointer function type
//获取缩略图数据流
typedef int (*DLLGetThumnailImagePathFunc)(const char*, unsigned char**, KF_INT32*, KF_INT32*, KF_INT32*); //Define pointer function type
//获取预览图数据流
typedef int (*DLLGetPriviewInfoPathFunc)(const char*, unsigned char**, KF_INT32*, KF_INT32*, KF_INT32*); //Define pointer function type
//获取标签图数据流
typedef int (*DLLGetLableInfoPathFunc)(const char*, unsigned char**, KF_INT32*, KF_INT32*, KF_INT32*); //Define pointer function type

typedef int (*DLLDeleteImageDataFunc)(LPVOID); //Define pointer function type
typedef int (*DLLUnInitImageFileFunc)(ImageInfoStruct*); //Define pointer function type


const char KFB_LIB_PATH[100] = "/usr/local/lib/libImageOperationLib.so";



#ifdef __cplusplus
}
#endif

#endif /* KFB_READER_H */

