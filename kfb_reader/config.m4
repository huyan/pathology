dnl $Id$
dnl config.m4 for extension kfb_reader

dnl Comments in this file start with the string 'dnl'.
dnl Remove where necessary. This file will not work
dnl without editing.

dnl If your extension references something external, use with:

 PHP_ARG_WITH(kfb_reader, for kfb_reader support,
 Make sure that the comment is aligned:
 [  --with-kfb_reader             Include kfb_reader support])

dnl Otherwise use enable:

dnl PHP_ARG_ENABLE(kfb_reader, whether to enable kfb_reader support,
dnl Make sure that the comment is aligned:
dnl [  --enable-kfb_reader           Enable kfb_reader support])

if test "$PHP_KFB_READER" != "no"; then
  dnl Write more examples of tests here...

  dnl # --with-kfb_reader -> check with-path
  dnl SEARCH_PATH="/usr/local /usr"     # you might want to change this
  dnl SEARCH_FOR="/include/kfb_reader.h"  # you most likely want to change this
  dnl if test -r $PHP_KFB_READER/$SEARCH_FOR; then # path given as parameter
  dnl   KFB_READER_DIR=$PHP_KFB_READER
  dnl else # search default path list
  dnl   AC_MSG_CHECKING([for kfb_reader files in default path])
  dnl   for i in $SEARCH_PATH ; do
  dnl     if test -r $i/$SEARCH_FOR; then
  dnl       KFB_READER_DIR=$i
  dnl       AC_MSG_RESULT(found in $i)
  dnl     fi
  dnl   done
  dnl fi
  dnl
  dnl if test -z "$KFB_READER_DIR"; then
  dnl   AC_MSG_RESULT([not found])
  dnl   AC_MSG_ERROR([Please reinstall the kfb_reader distribution])
  dnl fi

  dnl # --with-kfb_reader -> add include path
  dnl PHP_ADD_INCLUDE($KFB_READER_DIR/include)

  dnl # --with-kfb_reader -> check for lib and symbol presence
  dnl LIBNAME=kfb_reader # you may want to change this
  dnl LIBSYMBOL=kfb_reader # you most likely want to change this 

  dnl PHP_CHECK_LIBRARY($LIBNAME,$LIBSYMBOL,
  dnl [
  dnl   PHP_ADD_LIBRARY_WITH_PATH($LIBNAME, $KFB_READER_DIR/$PHP_LIBDIR, KFB_READER_SHARED_LIBADD)
  dnl   AC_DEFINE(HAVE_KFB_READERLIB,1,[ ])
  dnl ],[
  dnl   AC_MSG_ERROR([wrong kfb_reader lib version or lib not found])
  dnl ],[
  dnl   -L$KFB_READER_DIR/$PHP_LIBDIR -lm
  dnl ])
  dnl
  dnl PHP_SUBST(KFB_READER_SHARED_LIBADD)

  PHP_NEW_EXTENSION(kfb_reader, kfb_reader.c, $ext_shared,, -DZEND_ENABLE_STATIC_TSRMLS_CACHE=1)
fi
