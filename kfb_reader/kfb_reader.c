/*
  +----------------------------------------------------------------------+
  | PHP Version 7                                                        |
  +----------------------------------------------------------------------+
  | Copyright (c) 1997-2017 The PHP Group                                |
  +----------------------------------------------------------------------+
  | This source file is subject to version 3.01 of the PHP license,      |
  | that is bundled with this package in the file LICENSE, and is        |
  | available through the world-wide-web at the following url:           |
  | http://www.php.net/license/3_01.txt                                  |
  | If you did not receive a copy of the PHP license and are unable to   |
  | obtain it through the world-wide-web, please send a note to          |
  | license@php.net so we can mail you a copy immediately.               |
  +----------------------------------------------------------------------+
  | Author:                                                              |
  +----------------------------------------------------------------------+
 */

/* $Id$ */

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include "php.h"
#include "php_ini.h"
#include "ext/standard/info.h"
#include "kfb_reader.h"
#include "php_kfb_reader.h"
#include <dlfcn.h>


/* If you declare any globals in php_kfb_reader.h uncomment this:*/
ZEND_DECLARE_MODULE_GLOBALS(kfb_reader)


/* True global resources - no need for thread safety here */
static int le_kfb_reader;

/* {{{ PHP_INI
 */
/* Remove comments and fill if you need to have entries in php.ini
PHP_INI_BEGIN()
    STD_PHP_INI_ENTRY("kfb_reader.global_value",      "42", PHP_INI_ALL, OnUpdateLong, global_value, zend_kfb_reader_globals, kfb_reader_globals)
    STD_PHP_INI_ENTRY("kfb_reader.global_string", "foobar", PHP_INI_ALL, OnUpdateString, global_string, zend_kfb_reader_globals, kfb_reader_globals)
PHP_INI_END()
 */
/* }}} */

/* Remove the following function when you have successfully modified config.m4
   so that your module can be compiled into PHP, it exists only for testing
   purposes. */

/* Every user-visible function in PHP should document itself in the source */

/* {{{ proto string confirm_kfb_reader_compiled(string arg)
   Return a string to confirm that the module is compiled in */
PHP_FUNCTION(confirm_kfb_reader_compiled) {
    char *arg = NULL;
    size_t arg_len, len;
    zend_string *strg;

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &arg, &arg_len) == FAILURE) {
        return;
    }

    strg = strpprintf(0, "Congratulations! You have successfully modified ext/%.78s/config.m4. Module %.78s is now compiled into PHP.", "kfb_reader", arg);

    RETURN_STR(strg);
}
/* }}} */

/* The previous line is meant for vim and emacs, so it can correctly fold and
   unfold functions in source code. See the corresponding marks just before
   function definition, where the functions purpose is also documented. Please
   follow this convention for the convenience of others editing your code.
 */


PHP_FUNCTION(kfb_header) {
    //初始化图像
    DLLInitImageFileFunc InitImageFile; //Define function pointer variables
    //获取头信息
    DLLGetHeaderInfoFunc GetHeaderInfo; //Define function pointer variables



    DLLUnInitImageFileFunc UnInitImageFile; //Define function pointer variables

    KF_INT32* khiImageHeight;
    KF_INT32* khiImageWidth;
    KF_INT32* khiScanScale;
    float* khiSpendTime;
    double* khiScanTime;
    float* khiImageCapRes;
    KF_INT32* khiImageBlockSize;

    KF_INT32 ret;
    ImageInfoStruct* sImageInfo;

    char *kfbPath;
    size_t kfbPath_len;

    sImageInfo = (ImageInfoStruct*) emalloc(sizeof (ImageInfoStruct));
    khiImageHeight = (KF_INT32*) emalloc(sizeof (KF_INT32));
    khiImageWidth = (KF_INT32*) emalloc(sizeof (KF_INT32));
    khiScanScale = (KF_INT32*) emalloc(sizeof (KF_INT32));
    khiSpendTime = (float*) emalloc(sizeof (float));
    khiScanTime = (double*) emalloc(sizeof (double));
    khiImageCapRes = (float*) emalloc(sizeof (float));
    khiImageBlockSize = (KF_INT32*) emalloc(sizeof (KF_INT32));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &kfbPath, &kfbPath_len) == FAILURE) {
        return;
    }
    *(void* *) &InitImageFile = dlsym(KFB_G(handle), "InitImageFileFunc"); //Get open file function
    if (InitImageFile == NULL)
        return;
    ret = InitImageFile(sImageInfo, kfbPath);
    //ret=InitImageFile(sImageInfo,"/home/rick/sdk/123456-b_2017-06-27-14_35_18.kfb");
    if (ret == 0) {
        return;
    }

    *(void* *) &GetHeaderInfo = dlsym(KFB_G(handle), "GetHeaderInfoFunc"); //Get open file function
    if (GetHeaderInfo == NULL)
        return;


    GetHeaderInfo(*sImageInfo, khiImageHeight, khiImageWidth, khiScanScale, khiSpendTime, khiScanTime, khiImageCapRes, khiImageBlockSize); //Get header information
    //php_printf("\nwidth=%d;height=%d;scale=%d;spendtime=%f;scantime=%f;CapRes=%f;size=%d\n", *khiImageWidth, *khiImageHeight, *khiScanScale, *khiSpendTime, *khiScanTime, *khiImageCapRes, *khiImageBlockSize);
    //strg = strpprintf(0, "width=%d;height=%d;scale=%d;spendtime=%f;scantime=%f;CapRes=%f;size=%d", *khiImageWidth, *khiImageHeight, *khiScanScale, *khiSpendTime, *khiScanTime, *khiImageCapRes, *khiImageBlockSize);
    array_init(return_value);
    //    zval *return_value =NULL;
    //    ALLOC_ZVAL(return_value);
    //add_next_index_long(return_value, i);
    //    add_index_long(return_value,8,123);
    add_assoc_long(return_value, "width", *khiImageWidth);
    add_assoc_long(return_value, "height", *khiImageHeight);
    add_assoc_double(return_value, "scale", *khiScanScale);
    add_assoc_double(return_value, "spendtime", *khiSpendTime);
    add_assoc_double(return_value, "scantime", *khiScanTime);
    add_assoc_double(return_value, "capres", *khiImageCapRes);
    add_assoc_long(return_value, "size", *khiImageBlockSize);

    *(void* *) &UnInitImageFile = dlsym(KFB_G(handle), "UnInitImageFileFunc"); //Get open file function
    UnInitImageFile(sImageInfo); //Release memory
}

PHP_FUNCTION(kfb_roi_stream) {
    DLLInitImageFileFunc InitImageFile; //Define function pointer variables
    //获取指定区域图像数据流
    DLLGetImageDataRoiFunc GetImageDataRoiStream;
    DLLDeleteImageDataFunc DeleteImageData;
    DLLUnInitImageFileFunc UnInitImageFile;

    KF_INT32 ret;
    ImageInfoStruct* sImageInfo;
    unsigned char** ImageData;
    KF_INT32* nDataLength;

    char *kfbPath;
    size_t kfbPath_len;
    double dScale;
    long posX, posY, width, height;

    ImageData = (unsigned char**) emalloc(sizeof (unsigned char*));
    nDataLength = (KF_INT32*) emalloc(sizeof (KF_INT32));
    sImageInfo = (ImageInfoStruct*) emalloc(sizeof (ImageInfoStruct));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "sdllll",
            &kfbPath, &kfbPath_len, &dScale, &posX, &posY, &width, &height) == FAILURE) {
        return;
    }

    *(void* *) &InitImageFile = dlsym(KFB_G(handle), "InitImageFileFunc");
    if (InitImageFile == NULL) {
        return;
    }
    *(void* *) &GetImageDataRoiStream = dlsym(KFB_G(handle), "GetImageDataRoiFunc");

    ret = InitImageFile(sImageInfo, kfbPath);
    if (ret == 0) {
        return;
    }

    GetImageDataRoiStream(*sImageInfo, dScale, posX, posY, width, height, ImageData, nDataLength, 1);
    //    GetImageDataRoiStream(*sImageInfo, scale, sp_x, sp_y, nWidth, nHeight, ImageData, nDataLength, flag);
    if (0 == *nDataLength) {
        return;
    }
    array_init(return_value);
    add_assoc_long(return_value, "length", *nDataLength);
    add_assoc_stringl(return_value, "content", *ImageData, *nDataLength);
    *(void* *) &DeleteImageData = dlsym(KFB_G(handle), "DeleteImageDataFunc");
    DeleteImageData(*ImageData);
    *(void* *) &UnInitImageFile = dlsym(KFB_G(handle), "UnInitImageFileFunc"); //Get open file function
    UnInitImageFile(sImageInfo); //Release memory
}

PHP_FUNCTION(kfb_256_stream) {
    DLLInitImageFileFunc InitImageFile; //Define function pointer variables
    //获取图像流
    DLLGetImageStreamFunc GetImageStream; //Define function pointer variables
    DLLDeleteImageDataFunc DeleteImageData;
    DLLUnInitImageFileFunc UnInitImageFile;

    KF_INT32 ret;
    ImageInfoStruct* sImageInfo;
    unsigned char** ImageData;
    KF_INT32* nDataLength;

    char *kfbPath;
    size_t kfbPath_len;
    double dScale;
    long posX, posY;

    ImageData = (unsigned char**) emalloc(sizeof (unsigned char*));
    nDataLength = (KF_INT32*) emalloc(sizeof (KF_INT32));
    sImageInfo = (ImageInfoStruct*) emalloc(sizeof (ImageInfoStruct));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "sdll", &kfbPath, &kfbPath_len, &dScale, &posX, &posY) == FAILURE) {
        return;
    }

    *(void* *) &InitImageFile = dlsym(KFB_G(handle), "InitImageFileFunc");
    if (InitImageFile == NULL) {
        return;
    }
    *(void* *) &GetImageStream = dlsym(KFB_G(handle), "GetImageStreamFunc");

    ret = InitImageFile(sImageInfo, kfbPath);
    if (ret == 0) {
        return;
    }

    GetImageStream(sImageInfo, dScale, posX, posY, nDataLength, ImageData);
    if (0 == *nDataLength) {
        return;
    }
    array_init(return_value);
    add_assoc_long(return_value, "length", *nDataLength);
    add_assoc_stringl(return_value, "content", *ImageData, *nDataLength);

    *(void* *) &DeleteImageData = dlsym(KFB_G(handle), "DeleteImageDataFunc");
    DeleteImageData(*ImageData);
    *(void* *) &UnInitImageFile = dlsym(KFB_G(handle), "UnInitImageFileFunc"); //Get open file function
    UnInitImageFile(sImageInfo);
}

PHP_FUNCTION(kfb_thumbnail_stream) {
    //获取缩略图数据流
    DLLGetThumnailImagePathFunc GetThumnailImage;
    DLLDeleteImageDataFunc DeleteImageData;

    unsigned char** ImageData;
    KF_INT32* nDataLength;
    KF_INT32* nThumWidth, *nThumHeght;

    char *kfbPath;
    size_t kfbPath_len;

    ImageData = (unsigned char**) emalloc(sizeof (unsigned char*));
    nDataLength = (KF_INT32*) emalloc(sizeof (KF_INT32));
    nThumWidth = (KF_INT32*) emalloc(sizeof (KF_INT32));
    nThumHeght = (KF_INT32*) emalloc(sizeof (KF_INT32));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &kfbPath, &kfbPath_len) == FAILURE) {
        return;
    }

    *(void* *) &GetThumnailImage = dlsym(KFB_G(handle), "GetThumnailImagePathFunc");

    GetThumnailImage(kfbPath, ImageData, nDataLength, nThumWidth, nThumHeght);
    if (0 == *nDataLength) {
        return;
    }
    array_init(return_value);
    add_assoc_long(return_value, "length", *nDataLength);
    add_assoc_long(return_value, "width", *nThumWidth);
    add_assoc_long(return_value, "height", *nThumHeght);
    add_assoc_stringl(return_value, "content", *ImageData, *nDataLength);
    *(void* *) &DeleteImageData = dlsym(KFB_G(handle), "DeleteImageDataFunc");
    DeleteImageData(*ImageData);
}

PHP_FUNCTION(kfb_priview_stream) {
    //获取预览图数据流
    DLLGetPriviewInfoPathFunc GetPriviewInfoPath;
    DLLDeleteImageDataFunc DeleteImageData;

    unsigned char** ImageData;
    KF_INT32* nDataLength;
    KF_INT32* width, *height;

    char *kfbPath;
    size_t kfbPath_len;

    ImageData = (unsigned char**) emalloc(sizeof (unsigned char*));
    nDataLength = (KF_INT32*) emalloc(sizeof (KF_INT32));
    width = (KF_INT32*) emalloc(sizeof (KF_INT32));
    height = (KF_INT32*) emalloc(sizeof (KF_INT32));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &kfbPath, &kfbPath_len) == FAILURE) {
        return;
    }

    *(void* *) &GetPriviewInfoPath = dlsym(KFB_G(handle), "GetPriviewInfoPathFunc");

    GetPriviewInfoPath(kfbPath, ImageData, nDataLength, width, height);
    if (0 == *nDataLength) {
        return;
    }
    array_init(return_value);
    add_assoc_long(return_value, "length", *nDataLength);
    add_assoc_long(return_value, "width", *width);
    add_assoc_long(return_value, "height", *height);
    add_assoc_stringl(return_value, "content", *ImageData, *nDataLength);
    *(void* *) &DeleteImageData = dlsym(KFB_G(handle), "DeleteImageDataFunc");
    DeleteImageData(*ImageData);
}

PHP_FUNCTION(kfb_label_stream) {
    //获取标签图数据流
    DLLGetLableInfoPathFunc GetLableInfoPath;
    DLLDeleteImageDataFunc DeleteImageData;

    unsigned char** ImageData;
    KF_INT32* nDataLength;
    KF_INT32* width, *height;

    char *kfbPath;
    size_t kfbPath_len;

    ImageData = (unsigned char**) emalloc(sizeof (unsigned char*));
    nDataLength = (KF_INT32*) emalloc(sizeof (KF_INT32));
    width = (KF_INT32*) emalloc(sizeof (KF_INT32));
    height = (KF_INT32*) emalloc(sizeof (KF_INT32));

    if (zend_parse_parameters(ZEND_NUM_ARGS(), "s", &kfbPath, &kfbPath_len) == FAILURE) {
        return;
    }

    *(void* *) &GetLableInfoPath = dlsym(KFB_G(handle), "GetLableInfoPathFunc");

    GetLableInfoPath(kfbPath, ImageData, nDataLength, width, height);
    if (0 == *nDataLength) {
        return;
    }
    array_init(return_value);
    add_assoc_long(return_value, "length", *nDataLength);
    add_assoc_long(return_value, "width", *width);
    add_assoc_long(return_value, "height", *height);
    add_assoc_stringl(return_value, "content", *ImageData, *nDataLength);
    *(void* *) &DeleteImageData = dlsym(KFB_G(handle), "DeleteImageDataFunc");
    DeleteImageData(*ImageData);
}
/* {{{ php_kfb_reader_init_globals
 */

/* Uncomment this function if you have INI entries*/
static void php_kfb_reader_init_globals(zend_kfb_reader_globals *kfb_reader_globals) {
    kfb_reader_globals->handle = dlopen(KFB_LIB_PATH, RTLD_LAZY);
}

/* }}} */

/* {{{ PHP_MINIT_FUNCTION
 */
PHP_MINIT_FUNCTION(kfb_reader) {
    /* If you have INI entries, uncomment these lines
    REGISTER_INI_ENTRIES();
     */
    ZEND_INIT_MODULE_GLOBALS(kfb_reader, php_kfb_reader_init_globals, NULL);
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MSHUTDOWN_FUNCTION
 */
PHP_MSHUTDOWN_FUNCTION(kfb_reader) {
    /* uncomment this line if you have INI entries
    UNREGISTER_INI_ENTRIES();
     */
    return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request start */

/* {{{ PHP_RINIT_FUNCTION
 */
PHP_RINIT_FUNCTION(kfb_reader) {
#if defined(COMPILE_DL_KFB_READER) && defined(ZTS)
    ZEND_TSRMLS_CACHE_UPDATE();
#endif
    return SUCCESS;
}
/* }}} */

/* Remove if there's nothing to do at request end */

/* {{{ PHP_RSHUTDOWN_FUNCTION
 */
PHP_RSHUTDOWN_FUNCTION(kfb_reader) {
    return SUCCESS;
}
/* }}} */

/* {{{ PHP_MINFO_FUNCTION
 */
PHP_MINFO_FUNCTION(kfb_reader) {
    php_info_print_table_start();
    php_info_print_table_header(2, "kfb_reader support", "enabled");
    php_info_print_table_end();

    /* Remove comments if you have entries in php.ini
    DISPLAY_INI_ENTRIES();
     */
}
/* }}} */

/* {{{ kfb_reader_functions[]
 *
 * Every user visible function must have an entry in kfb_reader_functions[].
 */
const zend_function_entry kfb_reader_functions[] = {
    //	PHP_FE(confirm_kfb_reader_compiled,	NULL)		/* For testing, remove later. */
    PHP_FE(kfb_header, NULL) /* For testing, remove later. */
    PHP_FE(kfb_256_stream, NULL) /* For testing, remove later. */
    PHP_FE(kfb_roi_stream, NULL) /* For testing, remove later. */
    PHP_FE(kfb_thumbnail_stream, NULL) /* For testing, remove later. */
    PHP_FE(kfb_priview_stream, NULL) /* For testing, remove later. */
    PHP_FE(kfb_label_stream, NULL) /* For testing, remove later. */
    PHP_FE_END /* Must be the last line in kfb_reader_functions[] */
};
/* }}} */

/* {{{ kfb_reader_module_entry
 */
zend_module_entry kfb_reader_module_entry = {
    STANDARD_MODULE_HEADER,
    "kfb_reader",
    kfb_reader_functions,
    PHP_MINIT(kfb_reader),
    PHP_MSHUTDOWN(kfb_reader),
    PHP_RINIT(kfb_reader), /* Replace with NULL if there's nothing to do at request start */
    PHP_RSHUTDOWN(kfb_reader), /* Replace with NULL if there's nothing to do at request end */
    PHP_MINFO(kfb_reader),
    PHP_KFB_READER_VERSION,
    STANDARD_MODULE_PROPERTIES
};
/* }}} */

#ifdef COMPILE_DL_KFB_READER
#ifdef ZTS
ZEND_TSRMLS_CACHE_DEFINE()
#endif
ZEND_GET_MODULE(kfb_reader)
#endif

/*
 * Local variables:
 * tab-width: 4
 * c-basic-offset: 4
 * End:
 * vim600: noet sw=4 ts=4 fdm=marker
 * vim<600: noet sw=4 ts=4
 */
